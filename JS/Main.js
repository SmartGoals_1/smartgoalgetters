//Smartgoal Getters
//Noah V. Schijndel, Rens Poortvliet, Joris Herfst.


/*
Current list of sports (icons available):
-Archery
-Baseball
-Basketball
-Biking
-Fitness
-Football
-Golf
-Hiking
-Hockey
-Icehockey
-Iceskating
-Skating
-Swimming
-Tennis
-Volleyball
*/


//var locurl =  "/goalgetters/";
var locurl = "/smartgoalgetters/";

window.onload = function() {
	GetIEVersion();
	CheckForMobile();
	if ((window.location.href).includes("profile.php")) {
		GetProfileId();
	}
	else {
		GetUserData();
		GetTestimonialsData();
	}
}
function GetIEVersion() {
	var sAgent = window.navigator.userAgent;
	var Idx = sAgent.indexOf("MSIE");
	// If IE, return version number.
	if (Idx > 0) {
		$("#NormalStyle").attr("disabled", "disabled");
		//window.stop();
		$("body").empty();
		$("body").text("This site is not compatible with your current browser (Internet Explorer), Please use a different browser.");
	}
	// If IE 11 then look for Updated user agent string.
	else if (!!navigator.userAgent.match(/Trident\/7\./))  {
		$("#NormalStyle").attr("disabled", "disabled");
		//window.stop();
		$("body").empty();
		$("body").text("This site is not compatible with your current browser (Internet Explorer), Please use a different browser.");
	}
	else {

	}
}

function CheckForMobile() {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("#NormalStyle").attr("disabled", "disabled");
		$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', 'CSS/Mobile.css'));
	}
}

//Automatic scroll on start page.
$(function() {
	$('a ').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top + 1}, 600, 'linear');
	});
});

$(function() {
	$(".MainArea").hover(
		function() {
			$(this).children(".SlideoutOverlay").css("width","700px");
			$(this).children(".SlideoutOverlay").children(".SlideoutContent").fadeIn(1500);
		}, function() {
			$(this).children(".SlideoutOverlay").children(".SlideoutContent").css("display","none");
			$(this).children(".SlideoutOverlay").css("width","60px");
		}
		)
});

function BecomeAGetter() {
	//window.open('https://www.facebook.com/smartgoalssport/');
	StartOverlay();
}

function StartOverlay() {
	$("#RegistrationOverlay").fadeIn(600);
	$("#RegistrationOverlay").addClass("backgroundfadeinanimation");
	$("#RegistrationOverlay").removeClass("backgroundfadeoutanimation");
	$(".RegistrationFormDiv").fadeIn(600);
}

function ReturnOverlay() {
	$("#RegistrationOverlay").removeClass("backgroundfadeinanimation");
	$("#RegistrationOverlay").addClass("backgroundfadeoutanimation");
	$("#RegistrationOverlay").fadeOut(600);
	$(".RegistrationFormDiv").fadeOut(600);
	$(".RegistrationFormSportSelectionDiv").hide();
}

var ValueOn;
var CurrentSportSelection;
function FormSubmitShowSportsSelectionDiv(selection) {
	if (selection == "Sport1") { $('.RegistrationFormSportSelectionDiv').append('<style>.RegistrationFormSportSelectionDiv:after{margin-left: -40px;}</style>'); }
	if (selection == "Sport2") { $('.RegistrationFormSportSelectionDiv').append('<style>.RegistrationFormSportSelectionDiv:after{margin-left: 80px;}</style>'); }
	if (selection == "Sport3") { $('.RegistrationFormSportSelectionDiv').append('<style>.RegistrationFormSportSelectionDiv:after{margin-left: 190px;}</style>'); }
	CurrentSportSelection = selection;
	$(".RegistrationFormSportSelectionDiv").fadeIn(500);
	ValueOn = true;
	showImages();
}

function CloseSportsSelection(sportselection) {
	var GetSelectionClass = $("#"+CurrentSportSelection).attr("class");
	var GetSelectionClassSplit = GetSelectionClass.split(" ");
	if ((sportselection + "_svg") !== (GetSelectionClassSplit[1])) {
		$("#"+CurrentSportSelection).addClass(sportselection +"_svg").removeClass(GetSelectionClassSplit[1]);
	}
	$("#hiddenUser"+CurrentSportSelection).val(sportselection);
	showImages();
	$(".RegistrationFormSportSelectionDiv").fadeOut(500);
}

function showImages() {
	if (ValueOn == true) {
		ValueOn = false;
		$(".SVGHolderReg").each(function(index) {
			$(this).delay(50*index).fadeIn(500);
		});
	}
	else if (ValueOn == false) {
		ValueOn = true;
		$(".SVGHolderReg").each(function(index) {
			$(this).hide();
		});
	}
}

function SendApplication() {
	var UserFullName = $("#UserFullName").val();
	var UserEmail = $("#UserEmail").val();
	var UserLocation = $("#UserLocation").val();
	var UserOccupation = $("#UserOccupation").val();
	var UserFieldOfExpertise = $("#UserFieldOfExpertise").val();
	var UserAssociatedClub = $("#UserAssociatedClub").val();
	var UserBiography = $("#UserBiography").val();
	var UserWhyUseSG = $("#UserWhyUseSG").val();
	var UserSocialMedia = $("#UserSocialMedia").val();
	var UserSport1 = $("#hiddenUserSport1").val();
	var UserSport2 = $("#hiddenUserSport2").val();
	var UserSport3 = $("#hiddenUserSport3").val();

	if ((UserFullName !== "") && (UserEmail !== "") && (UserLocation !== "") && (UserOccupation !== "") && (UserFieldOfExpertise !== "") && (UserBiography !== "") && (UserWhyUseSG !== "")) {
		//Show loader2.
		$(".RegistrationFormInputWrapper").css("display","none");
		$(".RegistrationFormSportSelectionWrapper").css("display","none");
		$("#FormSaveButton").css("display","none");
		$(".loader2").css("display","block");

		var page = "database.php?submituser=true&UserFullName=" + UserFullName + "&UserEmail=" + UserEmail + "&UserLocation=" + UserLocation + "&UserOccupation=" + UserOccupation + "&UserFieldOfExpertise=" + UserFieldOfExpertise + "&UserAssociatedClub=" + UserAssociatedClub + "&UserBiography=" + UserBiography + "&UserWhyUseSG=" + UserWhyUseSG + "&UserSocialMedia=" + UserSocialMedia + "&UserSport1=" + UserSport1 + "&UserSport2=" + UserSport2 + "&UserSport3=" + UserSport3;
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            	//Finished.
            	$(".loader2").css("display","none");
            	$(".ContactMessage").css("display","block");
            }
        }
        xmlhttp.open("GET", page, true);
        xmlhttp.send();
    }
}

function isEven(n) {
	return n % 2 == 0;
}

//Profile redirect via ID.
function ProfileRedirect(id) {
	window.location.replace("https://www.smartgoalstraining.com/goalgetters/profile.php?UserID=" + id);
	//window.location.replace("http://localhost:8080/SmartGoalGetters/profile.php?UserID=" + id);
}

function GetUserData() {
	//Show loader.
	$(".loader").css("display","block");

	//Ajax call to get data.
	$.ajax({
		type: 'GET',
		url: 'database.php?loadusers=true',
		dataType: 'json',
		success: function(data) {
			console.log(data);
			LoadUserData(data);
		}
	});
}

//Load user data in Index.php
function LoadUserData(data) {
	var IndexPageHtmlString;
	var AlignSide;
	var dq = '"';

	for (i = 0; i < data.length; i++) {
		if (data[i].Accepted == 1) {
			//Check for left align / right align div.
			if (isEven(i) == true){
				AlignSide = "Left";
				IndexPageHtmlString =  "<div class='wow fadeInLeft LeftDiv'>";
			}
			else {
				AlignSide = "Right";
				IndexPageHtmlString =  "<div class='wow fadeInRight RightDiv'>";
			}
			IndexPageHtmlString += "<div class='ProfileWrapper'><div class='MainArea' id='MainAreaUser" + i + "' style='background-image: url(CSS/Images/Userfiles/"+ data[i].ProfilePicture +");'><div class='SlideoutOverlay'><span class='SlideoutArrow'></span><div class='SlideoutContent'>";
			IndexPageHtmlString += "<div class='ViewProfileOverlayButton'><a href='" + locurl + "profile.php?UserID=" + data[i].ID + "'><svg><rect x='0' y='0' fill='none' width='150' height='55'/></svg></a>My Profile</div>"
			IndexPageHtmlString += "<div class='SportsHolder'>";
			//Find user sports.
			var userSports = data[i].Sports.split(":");
			for (y = 0; y < userSports.length; y++) {
				if (y >= 3) {
					//Max 3 sports.
				}
				//Add sports svg's.
				else {
					if (userSports[y] == "Archery") { IndexPageHtmlString += "<div class='SVGHolder Archery_holder'><div class='svg Archery_svg'></div></div>"; }
					else if (userSports[y] == "Baseball") { IndexPageHtmlString += "<div class='SVGHolder Baseball_holder'><div class='svg Baseball_svg'></div></div>"; }
					else if (userSports[y] == "Basketball") { IndexPageHtmlString += "<div class='SVGHolder Basketball_holder'><div class='svg Basketball_svg'></div></div>"; }
					else if (userSports[y] == "Biking") { IndexPageHtmlString += "<div class='SVGHolder Biking_holder'><div class='svg Biking_svg'></div></div>"; }
					else if (userSports[y] == "Fitness") { IndexPageHtmlString += "<div class='SVGHolder Fitness_holder'><div class='svg Fitness_svg'></div></div>"; }
					else if (userSports[y] == "Football") { IndexPageHtmlString += "<div class='SVGHolder Football_holder'><div class='svg Football_svg'></div></div>"; }
					else if (userSports[y] == "Golf") { IndexPageHtmlString += "<div class='SVGHolder Golf_holder'><div class='svg Golf_svg'></div></div>"; }
					else if (userSports[y] == "Hiking") { IndexPageHtmlString += "<div class='SVGHolder Hiking_holder'><div class='svg Hiking_svg'></div></div>"; }
					else if (userSports[y] == "Hockey") { IndexPageHtmlString += "<div class='SVGHolder Hockey_holder'><div class='svg Hockey_svg'></div></div>"; }
					else if (userSports[y] == "Icehockey") { IndexPageHtmlString += "<div class='SVGHolder Icehockey_holder'><div class='svg Icehockey_svg'></div></div>"; }
					else if (userSports[y] == "Iceskating") { IndexPageHtmlString += "<div class='SVGHolder Iceskating_holder'><div class='svg Iceskating_svg'></div></div>"; }
					else if (userSports[y] == "Skating") { IndexPageHtmlString += "<div class='SVGHolder Skating_holder'><div class='svg Skating_svg'></div></div>"; }
					else if (userSports[y] == "Swimming") { IndexPageHtmlString += "<div class='SVGHolder Swimming_holder'><div class='svg Swimming_svg'></div></div>"; }
					else if (userSports[y] == "Tennis") { IndexPageHtmlString += "<div class='SVGHolder Tennis_holder'><div class='svg Tennis_svg'></div></div>"; }
					else if (userSports[y] == "Volleyball") { IndexPageHtmlString += "<div class='SVGHolder Volleyball_holder'><div class='svg Volleyball_svg'></div></div>"; }
				}
			}
			IndexPageHtmlString += "</div><div class='SportsHolderSeperator'></div><div class='InfoHolder'><div class='OverlaySmallText'>";
			IndexPageHtmlString += "<div class='LocationSmalltext'>Location:</div><div class='LocationHolder'><span>" + data[i].Location + "</span><br /></div>";
			IndexPageHtmlString += "<div class='JobSmalltext'>Field of expertise:</div><div class='JobHolder'><span>" + data[i].FieldOfExpertise + "</span><br /></div>";
			IndexPageHtmlString += "</div></div></div></div></div></div><div class='OverlayWrapper'>";
			IndexPageHtmlString += "<div class='NameOverlay'><span class='NameText'>" + data[i].Name + "</span></div>";
			IndexPageHtmlString += "<div class='SeperatorOverlay'><span class='SeperatorText'>-</span></div>";
			IndexPageHtmlString += "<div class='OccupationOverlay'><span class='OccupationText'>" + data[i].ShortOccupation + "</span></div></div></div>";
			
			//Hide loader.
			$(".loader").css("display","none");

			//Append user profile tab on left side.
			if (AlignSide == "Left") {
				$(".LeftFullDiv").append(IndexPageHtmlString);
			}
			//Append user profile tab on right side.
			else if (AlignSide == "Right") {
				$(".RightFullDiv").append(IndexPageHtmlString);
			}
			//Change profile pic values to fit box.
			if (i == 3) { $("#MainAreaUser"+i).css("background-size","58%"); }
		}
	}
}

function GetTestimonialsData() {
	//Ajax call to get data.
	$.ajax({
		type: 'GET',
		url: 'database.php?loadtestimonials=true',
		dataType: 'json',
		success: function(data) {
			LoadTestimonials(data);
		}
	});
}

function LoadTestimonials(data) {
	var TestimonialsPageHtmlString = "";
	var AlignSide = "Left";
	var AlignSideInner = "Left";

	for (i = 0; i < data.length; i++) {
		if (data[i].Accepted == 2) {
			//Check for left align / right align div.
			if (AlignSide == "Left") {
				if (AlignSideInner == "Left") {
					TestimonialsPageHtmlString += "<div class='TestimonialsLeftFullDiv'>";
					TestimonialsPageHtmlString += "<div class='TestimonialWrapper LeftDivTestimonials'>";
					TestimonialsPageHtmlString += "<div class='TestimonialTopSide'>";
					TestimonialsPageHtmlString += "<div class='TestimonialImage' style='background-image: url(CSS/Images/Userfiles/" + data[i].ProfilePicture + ");'></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialName'>" + data[i].Name + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialMiddle'><div class='TestimonialTitle'><span>Occupation:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].Occupation + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialBottomSide'><div class='TestimonialTitle'><span>Why do I train with SmartGoals:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].WhyDoITrain + "</div></div></div>";
					if (i == data.length) {	TestimonialsPageHtmlString += "</div>"; }
					AlignSideInner = "Right";
				}
				else if (AlignSideInner == "Right") {
					TestimonialsPageHtmlString += "<div class='TestimonialWrapper RightDivTestimonials'>";
					TestimonialsPageHtmlString += "<div class='TestimonialTopSide'>";
					TestimonialsPageHtmlString += "<div class='TestimonialImage' style='background-image: url(CSS/Images/Userfiles/" + data[i].ProfilePicture + ");'></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialName'>" + data[i].Name + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialMiddle'><div class='TestimonialTitle'><span>Occupation:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].Occupation + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialBottomSide'><div class='TestimonialTitle'><span>Why do I train with SmartGoals:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].WhyDoITrain + "</div></div></div></div>";
					AlignSideInner = "Left";
					AlignSide = "Right";
				}
			}
			else if (AlignSide == "Right") {
				if (AlignSideInner == "Left") {
					TestimonialsPageHtmlString += "<div class='TestimonialsRightFullDiv'>";
					TestimonialsPageHtmlString += "<div class='TestimonialWrapper LeftDivTestimonials'>";
					TestimonialsPageHtmlString += "<div class='TestimonialTopSide'>";
					TestimonialsPageHtmlString += "<div class='TestimonialImage' style='background-image: url(CSS/Images/Userfiles/" + data[i].ProfilePicture + ");'></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialName'>" + data[i].Name + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialMiddle'><div class='TestimonialTitle'><span>Occupation:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].Occupation + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialBottomSide'><div class='TestimonialTitle'><span>Why do I train with SmartGoals:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].WhyDoITrain + "</div></div></div>";
					if (i == data.length) {	TestimonialsPageHtmlString += "</div>"; }
					AlignSideInner = "Right";
				}
				else if (AlignSideInner == "Right") {
					TestimonialsPageHtmlString += "<div class='TestimonialWrapper RightDivTestimonials'>";
					TestimonialsPageHtmlString += "<div class='TestimonialTopSide'>";
					TestimonialsPageHtmlString += "<div class='TestimonialImage' style='background-image: url(CSS/Images/Userfiles/" + data[i].ProfilePicture + ");'></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialName'>" + data[i].Name + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialMiddle'><div class='TestimonialTitle'><span>Occupation:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].Occupation + "</div></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialBottomSide'><div class='TestimonialTitle'><span>Why do I train with SmartGoals:</span></div>";
					TestimonialsPageHtmlString += "<div class='TestimonialText'>" + data[i].WhyDoITrain + "</div></div></div></div>";
					AlignSideInner = "Left";
					AlignSide = "Left";
				}
			}
		}
	}
	$('#TestimonialsHolder').html(TestimonialsPageHtmlString);
}

function GetProfileId() {
	var Currenturl = window.location.href;
	var SplitUrl = Currenturl.split('?')[1].split('=');
	var UserID = SplitUrl[1];
	GetUserProfileData(UserID);
}

function GetUserProfileData(UserID) {
	//Show loader.
	$(".loader").css("display","block");
	//Ajax call to get data.
	$.ajax({
		type: 'GET',
		url: 'database.php?loaduserbyid=true&UserID=' + UserID,
		dataType: 'json',
		success: function(data) {
			LoadUserProfile(data);
		}
	});
}

function LoadUserProfile(data) {
	data = data[0];
	var dq = '"';
	var ProfilePageHtmlString;

	ProfilePageHtmlString = "<div class='ProfilePageTopSection'><div class='ProfilePictureArea'><div class='ProfileInfoPicture' style='background-image: url(CSS/Images/Userfiles/Thumbnails" + data.ProfilePicture + ");'></div></div>";
	ProfilePageHtmlString += "<div class='ProfileTitleArea'><div class='ProfileTitleSection ProfileTitleName'><span>" + data.Name + "</span></div>";
	ProfilePageHtmlString += "<div class='ProfileTitleSection ProfileTitleOccupation'><span>" + data.ShortOccupation + "</span></div></div></div>";
	ProfilePageHtmlString += "<div class='ProfilePageLowerSection'><div class='ProfileInfoTab'><div class='ProfileInfoSocialMediaTab'><div class='ProfileSocalMediaHolder'>";
	if (data.FacebookLink !== "") { ProfilePageHtmlString += "<div class='ProfileSocialMediaSVG' id='profilefacebooksvg' onclick='window.open(" + dq + data.FacebookLink + dq + ");'></div>"; }
	if (data.TwitterLink !== "") { ProfilePageHtmlString += "<div class='ProfileSocialMediaSVG' id='profiletwittersvg' onclick='window.open(" + dq + data.TwitterLink + dq + ");'></div>"; }
	if (data.InstagramLink !== "") { ProfilePageHtmlString += "<div class='ProfileSocialMediaSVG' id='profileinstagramsvg' onclick='window.open(" + dq + data.InstagramLink + dq + ");'></div>"; }
	if (data.LinkedinLink !== "") { ProfilePageHtmlString += "<div class='ProfileSocialMediaSVG' id='profilelinkedinsvg' onclick='window.open(" + dq + data.LinkedinLink + dq + ");'></div>"; }
	if (data.WebsiteLink !== "") { ProfilePageHtmlString += "<div class='ProfileSocialMediaSVG' id='profilewebsitesvg' onclick='window.open(" + dq + data.WebsiteLink + dq + ");'></div>"; }
	if (data.Email !== "") { ProfilePageHtmlString += "<div class='ProfileSocialMediaSVG' id='profileemailsvg' onclick='window.open(" + dq + data.Email + dq + ");'></div>"; }
	ProfilePageHtmlString += "</div></div><hr width='90%' id='ProfileHR1'><div class='ProfileInfoSection ProfileInfoSubinfo'>";
	ProfilePageHtmlString += "<div class='ProfileInfoTitle'><span>Location:</span></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoSubtext'><span>• " + data.Location + "</span></div></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoSection ProfileInfoSubinfo'><div class='ProfileInfoTitle'><span>Occupation:</span></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoSubtext'><span>• " + data.Occupation + "</span></div></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoSection ProfileInfoSubinfo' style='height:auto'>";
	ProfilePageHtmlString += "<div class='ProfileInfoTitle' style='height:20%'><span>Field of Expertise:</span></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoSubtext' style='height:80%; white-space:normal;'><span>• " + data.FieldOfExpertise + "</span></div></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoSection ProfileInfoClub'>";
	ProfilePageHtmlString += "<div class='ProfileInfoTitle'><span>Associated Club:</span></div><div class='ProfileInfoSubtext'><span>• " + data.AssociatedClub + "</span></div>";
	ProfilePageHtmlString += "<div class='ProfileInfoClubLogo' id='ProfileInfoAssociatedClubLogo' style='background-image: url(CSS/Images/Clublogos/" + data.AssociatedClubImage + ");'></div></div>";
	ProfilePageHtmlString += "<hr width='90%' id='ProfileHR3'><div class='ProfileInfoSportsTitle'><span>Sports:</span></div><div class='ProfileSportsWrapper'>";
	//Find user sports.
	var userSports = data.Sports;
	userSports = userSports.split(":");
	for (y = 0; y < userSports.length; y++) {
		if (y >= 3) {
			//Max 3 sports.			
		}
		//Add sports svg's.
		else {
			if (userSports[y] == "Archery") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileArchery_holder'><div class='Profilesvg ProfileArchery_svg'></div></div>"; }
			else if (userSports[y] == "Baseball") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileBaseball_holder'><div class='Profilesvg ProfileBaseball_svg'></div></div>"; }
			else if (userSports[y] == "Basketball") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileBasketball_holder'><div class='Profilesvg ProfileBasketball_svg'></div></div>"; }
			else if (userSports[y] == "Biking") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileBiking_holder'><div class='Profilesvg ProfileBiking_svg'></div></div>"; }
			else if (userSports[y] == "Fitness") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileFitness_holder'><div class='Profilesvg ProfileFitness_svg'></div></div>"; }
			else if (userSports[y] == "Football") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileFootball_holder'><div class='Profilesvg ProfileFootball_svg'></div></div>"; }
			else if (userSports[y] == "Golf") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileGolf_holder'><div class='Profilesvg ProfileGolf_svg'></div></div>"; }
			else if (userSports[y] == "Hiking") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileHiking_holder'><div class='Profilesvg ProfileHiking_svg'></div></div>"; }
			else if (userSports[y] == "Hockey") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileHockey_holder'><div class='Profilesvg ProfileHockey_svg'></div></div>"; }
			else if (userSports[y] == "Icehockey") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileIcehockey_holder'><div class='Profilesvg ProfileIcehockey_svg'></div></div>"; }
			else if (userSports[y] == "Iceskating") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileIceskating_holder'><div class='Profilesvg ProfileIceskating_svg'></div></div>"; }
			else if (userSports[y] == "Skating") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileSkating_holder'><div class='Profilesvg ProfileSkating_svg'></div></div>"; }
			else if (userSports[y] == "Swimming") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileSwimming_holder'><div class='Profilesvg ProfileSwimming_svg'></div></div>"; }
			else if (userSports[y] == "Tennis") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileTennis_holder'><div class='Profilesvg ProfileTennis_svg'></div></div>"; }
			else if (userSports[y] == "Volleyball") { ProfilePageHtmlString += "<div class='ProfileSVGHolder ProfileVolleyball_holder'><div class='Profilesvg ProfileVolleyball_svg'></div></div>"; }
		}
	}
	ProfilePageHtmlString += "</div></div><div class='ProfileBioTab'><div class='ProfileBioTitle'><span>Biography:</span></div>";
	ProfilePageHtmlString += "<div class='ProfileBioSubtext'>" + data.Biography;
	ProfilePageHtmlString += "</div><div class='ProfileBioTitle'><span>Why do I train with SmartGoals:</span></div>";
	ProfilePageHtmlString += "<div class='ProfileBioSubtext'>" + data.WhyDoITrain;
	ProfilePageHtmlString += "</div><div class='ProfileBioPicVideoSection'>";

	if (String(data.UserFile).includes(".mp4")) {
		ProfilePageHtmlString += "<video id='bgvid' playsinline muted controls><source src='CSS/Images/Userfiles/" + data.UserFile + "' type='video/mp4'></video></div></div></div>";
	}
	else if (String(data.UserFile).includes(".png")) {
		ProfilePageHtmlString += "<img src='CSS/Images/Userfiles/" +  data.UserFile + "'></div></div></div>";
	}
	else if (String(data.UserFile).includes(".jpg")) {
		ProfilePageHtmlString += "<img src='CSS/Images/Userfiles/" +  data.UserFile + "'></div></div></div>";
	}
	else {
		ProfilePageHtmlString += "<br /><br /><br />Video or image not found. <br /> Check file name (case sensitive).</div></div></div>";
	}
	//Set navigation arrows.
	if (data.PreviousID !== null) {
		$(".SlideoutArrowLeft").attr("onclick", "ProfileRedirect(" + data.PreviousID + ")");
		$(".SlideoutArrowLeft").css("display","block");
	}
	if (data.NextID !== null) {
		$(".SlideoutArrowRight").attr("onclick", "ProfileRedirect(" + data.NextID + ")");
		$(".SlideoutArrowRight").css("display","block");
	}
	//Hide loader.
	$(".loader").css("display","none");
	//Load into view.
	$('.ProfilePageInsert').html(ProfilePageHtmlString);
	//Call after load to add small changes.
	//IndividualProfileChanges(data);
}

function IndividualProfileChanges(data) {
	//Individual profile changes.
	if (data.ID == 1) { 
		$(".ProfileInfoPicture").css("background-size","150%"); 
	}
	if (data.ID == 2) {
		$(".ProfileInfoPicture").css("background-size","140%"); 
		$(".ProfileInfoPicture").css("background-position","left center");
	}
}