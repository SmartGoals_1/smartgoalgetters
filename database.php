<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=UTF-8');
$connect = mysqli_connect("localhost","smartgoalstraining_com_smartgoalgetters","no12ah","smartgoalstraining_com_smartgoalgetters");
if (isset($_GET['loadusers'])) {
	$RetrieveAll = "SELECT * FROM userdata";
	$RetrieveAllResult = mysqli_query($connect, $RetrieveAll);
	echo $connect;
	$SetToRows = array();
	while($r = mysqli_fetch_assoc($RetrieveAllResult)) {
		$SetToRows[] = $r;
	}
	echo json_encode(utf8ize($SetToRows));
}
if (isset($_GET['loadtestimonials'])) {
	$RetrieveAll = "SELECT * FROM userdata WHERE Accepted = 2";
	$RetrieveAllResult = mysqli_query($connect, $RetrieveAll);

	$SetToRows = array();
	while($r = mysqli_fetch_assoc($RetrieveAllResult)) {
		$SetToRows[] = $r;
	}
	echo json_encode(utf8ize($SetToRows));
}
if (isset($_GET['loaduserbyid'])) {
	$UserID = $_GET['UserID'];

	$RetrieveAll = "SELECT * FROM userdata WHERE ID = ". $UserID;
	$RetrieveAllResult = mysqli_query($connect, $RetrieveAll);

	$SetToRows = array();
	while($r = mysqli_fetch_assoc($RetrieveAllResult)) {
		$SetToRows[] = $r;
	}
	echo json_encode(utf8ize($SetToRows));
}

if (isset($_GET['submituser'])) {
	$UserFullName = $_GET['UserFullName'];
	$UserEmail = $_GET['UserEmail'];
	$UserLocation = $_GET['UserLocation'];
	$UserOccupation = $_GET['UserOccupation'];
	$UserFieldOfExpertise = $_GET['UserFieldOfExpertise'];
	$UserAssociatedClub = $_GET['UserAssociatedClub'];
	$UserBiography = $_GET['UserBiography'];
	$UserWhyUseSG = $_GET['UserWhyUseSG'];
	$UserSocialMedia = $_GET['UserSocialMedia'];
	$UserSport1 = $_GET['UserSport1'];
	$UserSport2 = $_GET['UserSport2'];
	$UserSport3 = $_GET['UserSport3'];
	$Sports;

	if ($UserSport1 !== "") {
		$Sports = "'". $UserSport1 ."'";
		if ($UserSport2 !== "") {
			$Sports = "'". $UserSport1 .":". $UserSport2 ."'";
			if ($UserSport3 !== "") {
				$Sports = "'". $UserSport1 .":". $UserSport2 .":". $UserSport3 ."'";
			}
		}
	}

	$NewUser = "INSERT INTO userdata (Accepted, Name, Email, Location, Sports, Occupation, FieldOfExpertise, AssociatedClub, Biography, WhyDoITrain, SocialMedia) VALUES ('0','". $UserFullName ."','". $UserEmail ."','". $UserLocation ."',". $Sports .",'". $UserOccupation ."','". $UserFieldOfExpertise ."','". $UserAssociatedClub ."','". $UserBiography ."','". $UserWhyUseSG ."','". $UserSocialMedia ."')";
	$NewUserResult = mysqli_query($connect, $NewUser);
}

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

mysqli_close($connect);
?>
