window.onload = function() {
	CheckForMobile();
	GetUserData();
}

function CheckForMobile() {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("#NormalStyle").attr("disabled", "disabled");
		window.stop();
		$("body").empty();
		$("body").text("This site is best viewed on a Computer or Laptop.");
	}
}

function ReturnOverlay() {
	$("#Overlay").removeClass("backgroundfadeinanimation");
	$("#Overlay").addClass("backgroundfadeoutanimation");
	$("#Overlay").fadeOut(600);
	$(".AcceptDeleteDiv").fadeOut(600);
}

function openHelp(url){
	var w=window.open(url);
	w.focus();
}

function GetUserData() {
	//Show loader.
	$(".loader").css("display","block");

	//Ajax call to get data.
	$.ajax({
		type: 'GET',
		url: 'database.php?loadusers=true',
		dataType: 'json',
		success: function(data) {
			LoadUserData(data);
		}
	});
}

function LoadUserData(data) {
	var DatabaseString;

	for (i = 0; i < data.length; i++) {
		DatabaseString += "<tr>";
		DatabaseString += "<td class='text-left'><div class='editbutton' id='editbutton"+i+"' onclick='editrow("+ i +")'>Edit</div><div class='deletebutton' onclick='deleterow("+ data[i].ID +")'>Delete</div></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].ID +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Accepted +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].PreviousID +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].NextID +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Name +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].ProfilePicture +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Location +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Sports +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Occupation +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].ShortOccupation +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].FieldOfExpertise +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].AssociatedClub +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].AssociatedClubImage +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Biography +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].WhyDoITrain +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].UserFile +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].SocialMedia +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].FacebookLink +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].TwitterLink +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].InstagramLink +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].LinkedinLink +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].WebsiteLink +"</p></td>";
		DatabaseString += "<td class='text-left'><p>"+ data[i].Email +"</p></td>";
		DatabaseString += "</tr>";
	}
	$("#SetTable").append(DatabaseString);

	$(".loader").css("display","none");
	$(".table-wrapper").css("display","block");
}

function editrow(id) {
	$("#SetTable tr:eq("+ id +") td").each(function(i) {
		if (i == 0) {
			$("#editbutton"+id).text("Save");
			$("#editbutton"+id).attr("onclick","saverow("+ id +")");
		}
		else {
	  		$("#SetTable tr:eq("+ id +") td:eq("+ i +")").html("<textarea>" + $("#SetTable tr:eq("+ id +") td:eq("+ i +")").text() + "</textarea>");
		}
	});
}

function saverow(id) {
	var saveddata = new Array();
	$("#SetTable tr:eq("+ id +") td").each(function(i) {
		if (i !== 0) {
			if ($("#SetTable tr:eq("+ id +") td:eq("+ i +") textarea").val() == "null") {
				saveddata.push(null);
			}
			else {
				saveddata.push($("#SetTable tr:eq("+ id +") td:eq("+ i +") textarea").val());
			}
		}
	});
	sendsavedrow(saveddata);
}

function sendsavedrow(saveddataarray) {
	var ID = saveddataarray[0];
	var Accepted = saveddataarray[1];
	var PreviousID = saveddataarray[2];
	var NextID = saveddataarray[3];
	var Name = saveddataarray[4];
	var ProfilePicture = saveddataarray[5];
	var Location = saveddataarray[6];
	var Sports = saveddataarray[7];
	var Occupation = saveddataarray[8];
	var ShortOccupation = saveddataarray[9];
	var FieldOfExpertise = saveddataarray[10];
	var AssociatedClub = saveddataarray[11];
	var AssociatedClubImage = saveddataarray[12];
	var Biography = saveddataarray[13];
	var WhyDoITrain = saveddataarray[14];
	var UserFile = saveddataarray[15];
	var SocialMedia = saveddataarray[16];
	var FacebookLink = saveddataarray[17];
	var TwitterLink = saveddataarray[18];
	var InstagramLink = saveddataarray[19];
	var LinkedinLink = saveddataarray[20];
	var WebsiteLink = saveddataarray[21];
	var Email = saveddataarray[22];

	var page = "database.php?savedata=true&ID="+ID+"&Accepted="+Accepted+"&PreviousID="+PreviousID+"&NextID="+NextID+"&Name="+Name+"&ProfilePicture="+ProfilePicture+"&Location="+Location+"&Sports="+Sports+"&Occupation="+Occupation+"&ShortOccupation="+ShortOccupation+"&FieldOfExpertise="+FieldOfExpertise+"&AssociatedClub="+AssociatedClub+"&AssociatedClubImage="+AssociatedClubImage+"&Biography="+Biography+"&WhyDoITrain="+WhyDoITrain+"&UserFile="+UserFile+"&SocialMedia="+SocialMedia+"&FacebookLink="+FacebookLink+"&TwitterLink="+TwitterLink+"&InstagramLink="+InstagramLink+"&LinkedinLink="+LinkedinLink+"&WebsiteLink="+WebsiteLink+"&Email="+Email;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
        	//Finished.
        	location.reload();
        }
    }
    xmlhttp.open("GET", page, true);
    xmlhttp.send();
}

function deleterow(id) {
	$("#Overlay").fadeIn(600);
	$("#Overlay").addClass("backgroundfadeinanimation");
	$("#Overlay").removeClass("backgroundfadeoutanimation");
	$(".AcceptDeleteDiv").fadeIn(600);

	$(".DeleteConfirmButton").attr("onclick","confirmdeleterow("+ id +")");
}

function confirmdeleterow(id) {
	var page = "database.php?deleteuser=true&ID=" + id;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
        	//Finished.
        	location.reload();
        }
    }
    xmlhttp.open("GET", page, true);
    xmlhttp.send();
}

function AddGoalgetter() {
	$("#SetTable").empty();
	$(".AddGoalGetter").css("background-color","lightgrey");
	var Addstring;

	Addstring += "<tr>";
	Addstring += "<td class='text-left'><div class='editbutton' onclick='addrow()'>Add</div></td>";
	Addstring += "<td class='text-left'></td>";
	Addstring += "<td class='text-left'><p></p></td>";
	Addstring += "<td class='text-left'><p></p></td>";
	Addstring += "<td class='text-left'><p></p></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "<td class='text-left'><textarea></textarea></td>";
	Addstring += "</tr>";

	$("#SetTable").append(Addstring);
}

function addrow() {
	var saveddata = new Array();
	$("#SetTable tr:eq(0) td").each(function(i) {
		if ((i !== 0) && (i !== 1)) {
			if ($("#SetTable tr:eq(0) td:eq("+ i +") textarea").val() == "null") {
				saveddata.push(null);
			}
			else if ($("#SetTable tr:eq(0) td:eq("+ i +") textarea").val() == undefined) {
				saveddata.push(0);
			}
			else {
				saveddata.push($("#SetTable tr:eq(0) td:eq("+ i +") textarea").val());
			}
		}
	});
	sendnewrow(saveddata);
}

function sendnewrow(saveddataarray) {
	var Accepted = saveddataarray[0];
	var PreviousID = saveddataarray[1];
	var NextID = saveddataarray[2];
	var Name = saveddataarray[3];
	var ProfilePicture = saveddataarray[4];
	var Location = saveddataarray[5];
	var Sports = saveddataarray[6];
	var Occupation = saveddataarray[7];
	var ShortOccupation = saveddataarray[8];
	var FieldOfExpertise = saveddataarray[9];
	var AssociatedClub = saveddataarray[10];
	var AssociatedClubImage = saveddataarray[11];
	var Biography = saveddataarray[12];
	var WhyDoITrain = saveddataarray[13];
	var UserFile = saveddataarray[14];
	var SocialMedia = saveddataarray[15];
	var FacebookLink = saveddataarray[16];
	var TwitterLink = saveddataarray[17];
	var InstagramLink = saveddataarray[18];
	var LinkedinLink = saveddataarray[19];
	var WebsiteLink = saveddataarray[20];
	var Email = saveddataarray[21];

	var page = "database.php?newgetter=true&Accepted="+Accepted+"&PreviousID="+PreviousID+"&NextID="+NextID+"&Name="+Name+"&ProfilePicture="+ProfilePicture+"&Location="+Location+"&Sports="+Sports+"&Occupation="+Occupation+"&ShortOccupation="+ShortOccupation+"&FieldOfExpertise="+FieldOfExpertise+"&AssociatedClub="+AssociatedClub+"&AssociatedClubImage="+AssociatedClubImage+"&Biography="+Biography+"&WhyDoITrain="+WhyDoITrain+"&UserFile="+UserFile+"&SocialMedia="+SocialMedia+"&FacebookLink="+FacebookLink+"&TwitterLink="+TwitterLink+"&InstagramLink="+InstagramLink+"&LinkedinLink="+LinkedinLink+"&WebsiteLink="+WebsiteLink+"&Email="+Email;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
        	//Finished.
        	location.reload();
        }
    }
    xmlhttp.open("GET", page, true);
    xmlhttp.send();
}