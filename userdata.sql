-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2018 at 09:05 AM
-- Server version: 5.5.49-log
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartgoalgetters`
--

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE IF NOT EXISTS `userdata` (
  `ID` int(11) NOT NULL,
  `Accepted` tinyint(1) NOT NULL,
  `PreviousID` int(11) DEFAULT NULL,
  `NextID` int(11) DEFAULT NULL,
  `Name` varchar(1000) NOT NULL,
  `ProfilePicture` varchar(1000) DEFAULT NULL,
  `Location` varchar(1000) NOT NULL,
  `Sports` varchar(1000) NOT NULL,
  `Occupation` varchar(1000) NOT NULL,
  `ShortOccupation` varchar(100) NOT NULL,
  `FieldOfExpertise` text NOT NULL,
  `AssociatedClub` varchar(1000) NOT NULL,
  `AssociatedClubImage` varchar(1000) DEFAULT NULL,
  `Biography` text NOT NULL,
  `WhyDoITrain` text NOT NULL,
  `UserFile` varchar(1000) DEFAULT NULL,
  `SocialMedia` text NOT NULL,
  `FacebookLink` varchar(1000) NOT NULL,
  `TwitterLink` varchar(1000) NOT NULL,
  `InstagramLink` varchar(1000) NOT NULL,
  `LinkedinLink` varchar(1000) NOT NULL,
  `WebsiteLink` varchar(1000) NOT NULL,
  `Email` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdata`
--

INSERT INTO `userdata` (`ID`, `Accepted`, `PreviousID`, `NextID`, `Name`, `ProfilePicture`, `Location`, `Sports`, `Occupation`, `ShortOccupation`, `FieldOfExpertise`, `AssociatedClub`, `AssociatedClubImage`, `Biography`, `WhyDoITrain`, `UserFile`, `SocialMedia`, `FacebookLink`, `TwitterLink`, `InstagramLink`, `LinkedinLink`, `WebsiteLink`, `Email`) VALUES
(1, 1, NULL, 2, 'Gavin Levey', 'gavinLevey.jpg', 'Aberdeen', 'Football', 'Head of Academy Coaching', 'Head of Academy Coaching', 'Youth development', 'Aberdeen Football Club', 'Aberdeen.png', 'Head of Academy Coaching at Aberdeen Football Club', 'Here at Aberdeen Football Club, SmartGoals are utilised to compliment our Academy coaching curriculum.  Awareness is one of the four main pillars in our individual player development programme, so the SmartGoals modern technology provides a unique system that forces our young players to recognise their surroundings and react quickly to changing situations, similar to the actual game.  As an ambitious club, we continue to seek innovative methods to help players and coaches develop their individual potential and I am confident that this technology can help us achieve long term gains.', 'AberdeenFCGavinLevey2.mp4', 'https://twitter.com/GavLevey', '', 'https://twitter.com/GavLevey', '', '', '', ''),
(2, 1, 1, 3, 'Eric Verboom', 'User2profileimage.jpg', 'Den Bosch', 'Hockey', 'Headcoach HC Den Bosch (Netherlands), Assistant Coach National Men''s Team Germany', 'Headcoach HC Den Bosch', 'Developing new exercises', 'HC Den Bosch', 'HCDenBosch.png', 'My name is Eric Verboom and I''m a field hockey coach. As a former player it made sense to follow my passion and become a coach. After different experiences in Holland (Nat. Men''s team), Belgium (Dragons), and Trinidad & Tobago, I became headcoach of HC Den Bosch and assistant of the National German men''s team.', 'I started using Smartgoals about 1,5 years ago and I''m still developing exercises with it every day. It gives me the opportunity to add different dimensions to my exercises. I use smartgoals in warming-ups, passing exercises, games, scoring drills and as start-up for exercises.', 'User2Vid.mp4', 'https://twitter.com/EricVerboom\r\nhttps://www.instagram.com/verboomeric/\r\nhttps://www.youtube.com/user/ericverboom010270/', '', 'https://twitter.com/EricVerboom', 'https://www.instagram.com/verboomeric/', '', 'https://www.youtube.com/user/ericverboom010270/', ''),
(3, 1, 2, 4, 'Timo Kleinhesselink', 'User1profileimage.jpg', 'Gelderland, De Heurne', 'Football', 'Owner of Sportbrein.com', 'Owner of Sportbrein.com', 'Cognitive Trainer', 'Longa’30, AZSV', 'Longa30.png', 'My name is Timo Kleinhesselink and I am the owner of Sportbrein. I started this company to train the brain of an athlete. I also write a blog every week about the brain and sports. The next step is to write an e-book.', 'I am using SmartGoals to train different brainfunctions. The most important one is Motor Inhibition. Research shows that: “The highly talented group showed superior motor inhibition as measured by stop signal reaction time (SSRT) on the Stop Signal task and a larger alerting effect on the Attention Network Test, indicating an enhanced ability to attain and maintain an alert state.”.', 'User1Vid.mp4', 'https://www.facebook.com/sportbrein/\r\nhttps://twitter.com/timoklh\r\nhttps://www.linkedin.com/in/timo-kleinhesselink-ba175346/\r\nhttp://sportbrein.com/', 'https://www.facebook.com/sportbrein/', 'https://twitter.com/timoklh', '', 'https://www.linkedin.com/in/timo-kleinhesselink-ba175346/', 'http://sportbrein.com/', ''),
(4, 1, 3, NULL, 'Carlo Kastermans', 'User3profileimage.jpg', 'Enschede', 'Football', 'Owner SportNav en Trainersboek, Manager Fitness Centre, Teacher Fitness Trainer A, Twentsche Voetbalschool, Technical youth coordinator amateur football', 'Owner of SportNav', 'Technical Trainer', '', NULL, 'I achieved a degree in sports and after this I have done many specialization studies. Since 1990 I work within the fitness and football industry.', 'I use SmartGoals for teams that appear to be on a dead track. We use them as testing equipment in which the players needs to improve his or hers awareness whilst keeping their technique level. This fits within our cognitive training program. This helps the players improve their automatism.', 'User3Vid.mp4', 'https://www.facebook.com/carlo.kastermans\r\nhttps://twitter.com/carlokastermans\r\nhttps://www.linkedin.com/in/carlo-kastermans-6808631a/', 'https://www.facebook.com/carlo.kastermans', 'https://twitter.com/carlokastermans', '', 'https://www.linkedin.com/in/carlo-kastermans-6808631a/', '', ''),
(5, 2, NULL, NULL, 'Rene Meulesteen', 'Renemeulesteen.jpg', '', '', 'Former assistant manager at Manchester United', '', '', '', NULL, '', 'You can use SmartGoals in so many ways, with the ball, without the ball, as a warm-up tool, passing exercises, positional games and very challenging and exciting small sided games. Therefore, SmartGoals are a fantastic addition to any football club at any level in the world.', NULL, '', '', '', '', '', '', ''),
(6, 2, NULL, NULL, 'Frank de Boer', 'boervi01.jpg', '', '', 'Former player of Ajax and FC Barcelona. Former coach of Ajax, Internazionale and Crystal Palace', '', '', '', NULL, '', 'Frank de Boer used SmartGoals at Ajax since the start of the company. We are proud to have Frank de Boer as one of the first users of the system. This is what he had to say about the product:\n\nTraining with SmartGoals improves spatial awareness of players. They need to know how to position themselves and how to turn in relation to this. They need to observe, process and act upon all of this, before they receive the ball that is coming towards them.\n', NULL, '', '', '', '', '', '', ''),
(7, 2, NULL, NULL, 'Hans van Breukelen', 'Hansvanbreukelen.jpg', '', '', 'Former technical director of KNVB and former international.', '', '', '', NULL, '', 'Within research I have performed it has been established that top players differentiate themselves from other players by two critical factors. They need to have spatial awareness, they need to process information quickly and learn quickly. Spatial awareness and processing speed are two things that you can train using SmartGoals. It is also tons of fun and something new!', NULL, '', '', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD UNIQUE KEY `ID_2` (`ID`),
  ADD UNIQUE KEY `ID_3` (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `userdata`
--
ALTER TABLE `userdata`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
