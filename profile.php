<!DOCTYPE html>
<html>
<head>
	<title>SmartGoal Getters</title>
	<meta charset="UTF-8">
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
	<meta name="keywords" content="Smartgoals smart goals getters"/>
	<meta name="description" content="Become a SmartGoal Getter now, and achieve the impossible!"/>
	<meta name="author" content="Noah V. Schijndel"/>
	<meta name="copyright" content=""/>
	<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
	<meta name="title" content="SmartGoalGetters">
	<meta name="distribution" content="global">
	<meta name="twitter:card" content="summary" />
	<meta name="linkedin:card" content="summary" />
	<meta property="og:url" content="https://www.smartgoalstraining.com/goalgetters/?1" />
	<meta property="og:title" content="SmartGoalGetters" />
	<meta property="og:image" content="https://www.smartgoalstraining.com/goalgetters/CSS/Images/ImageCard.jpg"/>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link href="CSS\Design.css" type="text/css" rel="stylesheet" id="NormalStyle"/>
	<link href="CSS\normalize.css" type="text/css" rel="stylesheet" />
	<link href="CSS\override.css" type="text/css" rel="stylesheet" />
	<link href="CSS\animate.css" type="text/css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Mukta+Mahee" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Biryani" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="JS/Main.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="CSS/WOW-master/dist/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<!-- Hotjar Tracking Code for noahvanschijndel.nl/smartgoalgetters -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:808614,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119743509-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119743509-2');
</script>

</head>
<body>
	<!-- http://localhost:8080/SmartGoalGetters/ -->
	<!-- http://noahvanschijndel.nl/smartgoalgetters -->
	<div class="headersection"><div class="headertop"><div id="headerlogo" onclick="window.location.href='https://smartgoalstraining.com/goalgetters/';"></div></div><div class="headerbottom"></div></div>
	<div class="ProfilePageWrapper">
		<span class='SlideoutArrowLeft'></span>
		<span class='SlideoutArrowRight'></span>
		<div class="ProfilePageInsert" style="min-height: 150px;"></div>
		<div class="loader"></div>
	<div class="ProfilePageFooterLogo FullDiv">
		<hr width="42%" id="hr7"><hr width="42%" id="hr8">
	</div>
	<div class="ProfilePageFooterLinks FullDiv">
		<div id="FooterInfo">
			<div id="InfoText">© 2018 SmartGoals</div>
			<div id="WebsiteLink" onclick="window.open('https://www.smartgoals.nl');">www.smartgoals.nl</div>
		</div>
		<div id="SocialMediaLinks">
			<div class="SocialMediaSVG" id="facebooksvg" onclick="window.open('https://www.facebook.com/smartgoalssport/');"></div>
			<div class="SocialMediaSVG" id="twittersvg" onclick="window.open('https://twitter.com/smartgoalssport');"></div>
			<div class="SocialMediaSVG" id="instagramsvg" onclick="window.open('https://www.instagram.com/smartgoalsfootball/');"></div>
			<div class="SocialMediaSVG" id="emailsvg" onclick="document.location.href = 'mailto:info@smartgoals.nl';"></div>
		</div>
	</div>
</div>
</body>
</html>