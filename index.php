<!DOCTYPE html>
<html>
<head>
	<title>SmartGoal Getters</title>
	<meta charset="UTF-8">
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
	<meta name="keywords" content="Smartgoals smart goals getters"/>
	<meta name="description" content="Become a SmartGoal Getter now, and achieve the impossible!"/>
	<meta name="author" content="Noah V. Schijndel"/>
	<meta name="copyright" content=""/>
	<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
	<meta name="title" content="SmartGoalGetters">
	<meta name="distribution" content="global">
	<meta name="twitter:card" content="summary" />
	<meta name="linkedin:card" content="summary" />
	<meta property="og:url" content="https://www.smartgoalstraining.com/goalgetters/?1" />
	<meta property="og:title" content="SmartGoalGetters" />
	<meta property="og:image" content="https://www.smartgoalstraining.com/goalgetters/CSS/Images/ImageCard.jpg"/>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link href="CSS\Design.css" type="text/css" rel="stylesheet" id="NormalStyle"/>
	<link href="CSS\normalize.css" type="text/css" rel="stylesheet" />
	<link href="CSS\override.css" type="text/css" rel="stylesheet" />
	<link href="CSS\animate.css" type="text/css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Mukta+Mahee" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Biryani" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="JS/Main.js"></script>
	<script src="CSS/WOW-master/dist/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:808614,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119743509-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119743509-2');
</script>

</head>
<body>
	<div class="headersection"><div class="headertop"><div id="headerlogo" onclick="location.reload();"></div></div><div class="headerbottom"></div></div>
	<div class="content">
		<div id="large" class="large"></div>
		<section id="section01" class="FirstSection">
			<a href="#section02"><span></span><p>Scroll</p></a>
			<div class="HeaderScrollSection"></div>
		</section>
	</div>
	<section id="section02"></section>

	<div class="MainContent">
		<div class="HeaderSeperator FullDiv"></div>
		<div class="AboutUsHeader FullDiv">
			<hr width="22%" id="hr1"><span id="AboutUsTitle">About Us</span><hr width="22%" id="hr2">
		</div>
		<div class="AboutUsText">We are SmartGoalGetters. We all try to improve, each in our own way. Getting better one training at a time. The creativity of the trainers is endless and we can all grow if we inspire each other. Take a look at the profiles of our users and see what they do to become better every day! Become a SmartGoalGetter and inspire others to get the most out of their training!</div>
		<div class="MeetTheTeamHeader FullDiv">
			<hr width="22%" id="hr3"><span id="MeetTheTeamTitle">Our Community</span><hr width="22%" id="hr4">
		</div>
		<div id="ProfilesWrapper" class="FullDiv" style="margin-top: 200px;">
			<div class="loader"></div>
			<div class="LeftFullDiv"></div>
			<div class="RightFullDiv"></div>
		</div>
		<div class="FullDiv" style="margin-top: 100px;margin-bottom: 550px;">
			<div class="TestimonialsHeader FullDiv">
				<hr width="22%" id="hr1"><span id="TestimonialsTitle">Testimonials</span><hr width="22%" id="hr2">
			</div>
			<div id="TestimonialsHolder"></div>
		</div>
		<div class="ApplyForGetter"><div class="ApplyForGetterButton" onclick="BecomeAGetter();">Become a SmartGoal Getter!</div></div>
		<div class="MainPageFooterLogo FullDiv">
			<hr width="42%" id="hr5"><hr width="42%" id="hr6">
		</div>
		<div class="MainPageFooterLinks FullDiv">
			<div id="FooterInfo">
				<div id="InfoText">© 2018 SmartGoals</div>
				<div id="WebsiteLink" onclick="window.open('https://www.smartgoals.nl');">www.smartgoals.nl</div>
			</div>
			<div id="SocialMediaLinks">
				<div class="SocialMediaSVG" id="facebooksvg" onclick="window.open('https://www.facebook.com/smartgoalssport/');"></div>
				<div class="SocialMediaSVG" id="twittersvg" onclick="window.open('https://twitter.com/smartgoalssport');"></div>
				<div class="SocialMediaSVG" id="instagramsvg" onclick="window.open('https://www.instagram.com/smartgoalsfootball/');"></div>
				<div class="SocialMediaSVG" id="emailsvg" onclick="document.location.href = 'mailto:info@smartgoals.nl';"></div>
			</div>
		</div>
	</div>
	<div class="RegistrationFormDiv">
		<div class="RegistrationFormDivCloseButton" onclick="ReturnOverlay();">X</div>
		<span id="RegistrationFormHeader">Become a SmartGoal Getter!</span><br />
		<hr width="80%" id="hr9">
		<div class="loader2"></div>
		<div class="ContactMessage">Thanks for applying! <br /> We will contact you as soon as possible.</div>
		<div class="RegistrationFormInputWrapper">
			<span class="RegistrationFormRequiredText">Items marked with * are required.</span>
			<input type=text id="UserFullName" style="margin-top:3px;" placeholder="Full name *" required><br />
			<input type=email id="UserEmail" placeholder="Email *" required><br />
			<input type=text id="UserLocation" placeholder="Location *" required><br />
			<input type=text id="UserOccupation" placeholder="Occupation *" required><br />
			<input type=text id="UserFieldOfExpertise" placeholder="Field of expertise *" required><br />
			<input type=text id="UserAssociatedClub" placeholder="Associated Club"><br />
			<span class="RegistrationFormRequiredText" style="font-weight:normal;margin-left:30px;">Select your sports. Leave empty for less then 3 sports.</span>
			<div class="RegistrationFormSportWrapper" onclick="FormSubmitShowSportsSelectionDiv('Sport3');"><input type=image id="Sport3" alt="" class="svg Empty_svg" style="-webkit-mask-size: 70%;"></div>
			<input type=text id="hiddenUserSport3" style="display:none;">
			<div class="RegistrationFormSportWrapper" onclick="FormSubmitShowSportsSelectionDiv('Sport2');"><input type=image id="Sport2" alt="" class="svg Empty_svg" style="-webkit-mask-size: 70%;"></div>
			<input type=text id="hiddenUserSport2" style="display:none;">
			<div class="RegistrationFormSportWrapper" onclick="FormSubmitShowSportsSelectionDiv('Sport1');"><input type=image id="Sport1" alt="" class="svg Empty_svg" style="-webkit-mask-size: 70%;"></div><br />
			<input type=text id="hiddenUserSport1" style="display:none;">
		</div>
		<div class="RegistrationFormSportSelectionWrapper">
			<textarea id="UserBiography" style="margin-top:32px;" placeholder="Biography *" required></textarea>
			<textarea id="UserWhyUseSG" style="margin-top:12px;" placeholder="Why do you use SmartGoals? *" required></textarea>
			<textarea id="UserSocialMedia" style="margin-top:11px;height: 108px;" placeholder="Enter your social media links here:"></textarea>
		</div>
		<input type="submit" value="Send Application" id="FormSaveButton" onclick="SendApplication('SubmitForm')">
	</div>
	<div class="RegistrationFormSportSelectionDiv">
		<div class="SVGHolderReg Archery_holder" style="width:25%;" onclick="CloseSportsSelection('Archery');">
			<div class="svg Archery_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Baseball_holder" style="width:25%;" onclick="CloseSportsSelection('Baseball');">
			<div class="svg Baseball_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Basketball_holder" style="width:25%;" onclick="CloseSportsSelection('Basketball');">
			<div class="svg Basketball_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Biking_holder" style="width:25%;" onclick="CloseSportsSelection('Biking');">
			<div class="svg Biking_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Fitness_holder" style="width:25%;" onclick="CloseSportsSelection('Fitness');">
			<div class="svg Fitness_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Football_holder" style="width:25%;" onclick="CloseSportsSelection('Football');">
			<div class="svg Football_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Golf_holder" style="width:25%;" onclick="CloseSportsSelection('Golf');">
			<div class="svg Golf_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Hiking_holder" style="width:25%;" onclick="CloseSportsSelection('Hiking');">
			<div class="svg Hiking_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Hockey_holder" style="width:25%;" onclick="CloseSportsSelection('Hockey');">
			<div class="svg Hockey_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Icehockey_holder" style="width:25%;" onclick="CloseSportsSelection('Icehockey');">
			<div class="svg Icehockey_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Iceskating_holder" style="width:25%;" onclick="CloseSportsSelection('Iceskating');">
			<div class="svg Iceskating_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Skating_holder" style="width:25%;" onclick="CloseSportsSelection('Skating');">
			<div class="svg Skating_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Swimming_holder" style="width:25%;" onclick="CloseSportsSelection('Swimming');">
			<div class="svg Swimming_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Tennis_holder" style="width:25%;" onclick="CloseSportsSelection('Tennis');">
			<div class="svg Tennis_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Volleyball_holder" style="width:25%;" onclick="CloseSportsSelection('Volleyball');">
			<div class="svg Volleyball_svg" style="-webkit-mask-size: 50%;background:#FF7F22;"></div>
		</div>
		<div class="SVGHolderReg Empty_holder" style="width:25%;" onclick="CloseSportsSelection('Empty');">
			<div class="svg Empty_svg" style="-webkit-mask-size: 40%;background:#FF7F22;"></div>
		</div>
	</div>
	<div id="RegistrationOverlay" onclick="ReturnOverlay();"></div>
</body>
</html>